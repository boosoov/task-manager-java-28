package com.rencredit.jschool.boruak.taskmanager.locator;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IEndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

@Getter
public class EndpointLocator implements IEndpointLocator {

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    private final AuthEndpointService authEndpointService = new AuthEndpointService();


    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @NotNull
    private final AuthEndpoint authEndpoint = authEndpointService.getAuthEndpointPort();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EndpointLocator that = (EndpointLocator) o;
        return userEndpointService.equals(that.userEndpointService) &&
                sessionEndpointService.equals(that.sessionEndpointService) &&
                taskEndpointService.equals(that.taskEndpointService) &&
                projectEndpointService.equals(that.projectEndpointService) &&
                adminEndpointService.equals(that.adminEndpointService) &&
                adminUserEndpointService.equals(that.adminUserEndpointService) &&
                authEndpointService.equals(that.authEndpointService) &&
                userEndpoint.equals(that.userEndpoint) &&
                sessionEndpoint.equals(that.sessionEndpoint) &&
                taskEndpoint.equals(that.taskEndpoint) &&
                projectEndpoint.equals(that.projectEndpoint) &&
                adminEndpoint.equals(that.adminEndpoint) &&
                adminUserEndpoint.equals(that.adminUserEndpoint) &&
                authEndpoint.equals(that.authEndpoint);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userEndpointService, sessionEndpointService, taskEndpointService, projectEndpointService, adminEndpointService, adminUserEndpointService, authEndpointService, userEndpoint, sessionEndpoint, taskEndpoint, projectEndpoint, adminEndpoint, adminUserEndpoint, authEndpoint);
    }

}
