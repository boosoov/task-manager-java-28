package com.rencredit.jschool.boruak.taskmanager.exception.incorrect;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class IncorrectHashPasswordException extends AbstractException {

    public IncorrectHashPasswordException() {
        super("Error! Hash password incorrect...");
    }

}
