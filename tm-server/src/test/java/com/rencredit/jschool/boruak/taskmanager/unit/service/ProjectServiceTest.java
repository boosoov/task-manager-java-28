package com.rencredit.jschool.boruak.taskmanager.unit.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistAbstractListException;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.repository.ProjectRepository;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import com.rencredit.jschool.boruak.taskmanager.service.PropertyService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import com.rencredit.jschool.boruak.taskmanager.util.EntityManagerFactoryUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Category(UnitTestCategory.class)
public class ProjectServiceTest {

    @NotNull final EntityManagerFactoryUtil entityManagerFactoryUtil = new EntityManagerFactoryUtil(new PropertyService());
    @NotNull ProjectRepository projectRepository;
    @NotNull ProjectService projectService;
    @NotNull IUserService userService;
    @Nullable String userId;

    @Before
    public void init() throws EmptyLoginException, EmptyHashLineException, BusyLoginException, EmptyRoleException, EmptyPasswordException, EmptyUserException, DeniedAccessException {
        projectRepository = new ProjectRepository();
        projectService = new ProjectService();
        userService = new UserService();
        userService.clearAll();
        userId = userService.getByLogin("1").getId();
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateUserIdNameWithoutUserId() throws EmptyNameException, EmptyUserIdException {
        projectService.create(null, "Demo");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateUserIdNameWithoutName() throws EmptyNameException, EmptyUserIdException {
        projectService.create(userId, (String) null);
    }

    @Test
    public void testCreateUserIdName() throws EmptyNameException, EmptyUserIdException {
        projectService.create(userId, "name");
        Assert.assertNotNull(projectService.findOneByName(userId, "name"));
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateUserIdNameDescriptionWithoutUserId() throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException {
        projectService.create(null, "Demo", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateUserIdNameDescriptionWithoutName() throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException {
        projectService.create(userId, null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeCreateUserIdNameDescriptionWithoutDescription() throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException {
        projectService.create(userId, "Demo", null);
    }

    @Test
    public void testCreateUserIdNameDescription() throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException {
        projectService.create(userId, "name", "description");
        Assert.assertNotNull(projectService.findOneByName(userId, "name"));
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateUserIdProjectDTOWithoutUserId() throws EmptyProjectException, EmptyUserIdException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(null, project);
    }

    @Test(expected = EmptyProjectException.class)
    public void testNegativeCreateUserIdProjectDTOWithoutProjectDTO() throws EmptyProjectException, EmptyUserIdException {
        projectService.create(userId, (ProjectDTO) null);
    }

    @Test
    public void testCreateUserIdProjectDTO() throws EmptyProjectException, EmptyUserIdException, EmptyNameException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        Assert.assertNotNull(projectService.findOneByName(userId, "name"));
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindAllByUserIdWithoutUserId() throws EmptyUserIdException {
        projectService.findAllByUserId(null);

    }

    @Test
    public void testFindAllByUserId() throws EmptyUserIdException, EmptyProjectException {
        @NotNull final List<ProjectDTO> emptyListProjectDTO = projectService.findAllByUserId(userId);
        Assert.assertTrue(emptyListProjectDTO.isEmpty());
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        @NotNull final List<ProjectDTO> notEmptyListProjectDTO = projectService.findAllByUserId(userId);
        Assert.assertFalse(notEmptyListProjectDTO.isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeClearAllByUserIdWithoutUserId() throws EmptyUserIdException {
        projectService.clearByUserId(null);

    }

    @Test
    public void testClearAllByUserId() throws EmptyUserIdException, EmptyProjectException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        @NotNull final List<ProjectDTO> notEmptyListProjectDTO = projectService.findAllByUserId(userId);
        Assert.assertFalse(notEmptyListProjectDTO.isEmpty());
        projectService.clearByUserId(userId);
        @NotNull final List<ProjectDTO> emptyListProjectDTO = projectService.findAllByUserId(userId);
        Assert.assertTrue(emptyListProjectDTO.isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneByIndexWithoutUserId() throws EmptyUserIdException, IncorrectIndexException, EmptyProjectException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        @Nullable final ProjectDTO findProjectDTO = projectService.findOneByIndex(null, 0);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeFindOneByIndexWithoutIndex() throws EmptyUserIdException, IncorrectIndexException, EmptyProjectException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        @Nullable final ProjectDTO findProjectDTO = projectService.findOneByIndex(userId, null);
    }

    @Test
    public void testFindOneByIndex() throws EmptyUserIdException, IncorrectIndexException, EmptyProjectException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        @Nullable final ProjectDTO findProjectDTO = projectService.findOneByIndex(userId, 0);
        Assert.assertEquals(project.getId(), findProjectDTO.getId());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneByNameWithoutUserId() throws EmptyProjectException, EmptyUserIdException, EmptyNameException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        @Nullable final ProjectDTO findProjectDTO = projectService.findOneByName(null, "name");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeFindOneByNameWithoutName() throws EmptyNameException, EmptyUserIdException, EmptyProjectException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        @Nullable final ProjectDTO findProjectDTO = projectService.findOneByName(userId, null);
    }

    @Test
    public void testFindOneByName() throws EmptyNameException, EmptyUserIdException, EmptyProjectException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        @Nullable final ProjectDTO findProjectDTO = projectService.findOneByName(userId, "name");
        Assert.assertEquals(project.getId(), findProjectDTO.getId());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneByIdWithoutUserId() throws EmptyIdException, EmptyUserIdException, EmptyProjectException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.findOneDTOById(null, project.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeFindOneByIdWithoutId() throws EmptyIdException, EmptyUserIdException, EmptyProjectException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.findOneDTOById(userId, null);
    }

    @Test
    public void testFindOneById() throws EmptyIdException, EmptyUserIdException, EmptyProjectException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        @Nullable final ProjectDTO findProjectDTO = projectService.findOneDTOById(userId, project.getId());
        Assert.assertEquals(project.getId(), findProjectDTO.getId());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateProjectDTOByIdWithoutUserId() throws EmptyIdException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.updateProjectById(null, userId, "name", "description");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateProjectDTOByIdWithoutId() throws EmptyIdException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.updateProjectById(userId, null, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateProjectDTOByIdWithoutName() throws EmptyIdException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.updateProjectById(userId, userId, null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateProjectDTOByIdWithoutDescription() throws EmptyIdException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.updateProjectById(userId, userId, "name", null);
    }

    @Test
    public void testUpdateProjectDTOById() throws EmptyIdException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        @NotNull final ProjectDTO updatedProjectDTO = projectService.updateProjectById(userId, project.getId(), "name2", "description2");
        Assert.assertEquals("name2", updatedProjectDTO.getName());
        Assert.assertEquals("description2", updatedProjectDTO.getDescription());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateProjectDTOByIndexWithoutUserId() throws IncorrectIndexException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.updateProjectByIndex(null, 0, "name", "description");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeUpdateProjectDTOByIndexWithoutIndex() throws IncorrectIndexException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.updateProjectByIndex(userId, null, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateProjectDTOByIndexWithoutName() throws IncorrectIndexException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.updateProjectByIndex(userId, 0, null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateProjectDTOByIndexWithoutDescription() throws IncorrectIndexException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.updateProjectByIndex(userId, 0, "name", null);
    }

    @SneakyThrows
    @Test
    public void testUpdateProjectDTOByIndex() throws IncorrectIndexException, EmptyNameException, EmptyUserIdException, EmptyProjectException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        @NotNull final ProjectDTO updatedProjectDTO = projectService.updateProjectByIndex(userId, 0, "name2", "description2");
        Assert.assertEquals("name2", updatedProjectDTO.getName());
        Assert.assertEquals("description2", updatedProjectDTO.getDescription());
    }

    @SneakyThrows
    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveWithoutUserId() throws EmptyProjectException, EmptyUserIdException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.remove(null, project);
    }

    @SneakyThrows
    @Test(expected = EmptyProjectException.class)
    public void testNegativeRemoveWithoutProjectDTO() throws EmptyProjectException, EmptyUserIdException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.remove(userId, null);
    }

    @SneakyThrows
    @Test
    public void testRemove() throws EmptyProjectException, EmptyUserIdException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.remove(userId, project);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIndexWithoutUserId() throws EmptyUserIdException, IncorrectIndexException, EmptyProjectException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.removeOneByIndex(null, 0);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeRemoveOneByIndexWithoutIndex() throws EmptyUserIdException, IncorrectIndexException, EmptyProjectException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.removeOneByIndex(userId, null);
    }

    @Test
    public void testRemoveOneByIndex() throws EmptyUserIdException, IncorrectIndexException, EmptyProjectException, EmptyNameException {
        @NotNull final User user = new User();
        @NotNull final Project project = new Project(user, "name");
        projectService.create(userId, project);
        projectService.removeOneByIndex(userId, 0);
        projectService.findOneByName(userId, "name");
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByNameWithoutUserId() throws EmptyNameException, EmptyUserIdException, EmptyProjectException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.removeOneByName(null, "name");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeRemoveOneByNameWithoutName() throws EmptyNameException, EmptyUserIdException, EmptyProjectException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.removeOneByName(userId, null);
    }

    @Test
    public void testRemoveOneByName() throws EmptyProjectException, EmptyUserIdException, EmptyNameException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.removeOneByName(userId, "name");
        Assert.assertNull(projectService.findOneByName(userId, "name"));
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIdWithoutUserId() throws EmptyIdException, EmptyUserIdException, EmptyProjectException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.removeOneById(null, project.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeRemoveOneByIdWithoutId() throws EmptyIdException, EmptyUserIdException, EmptyProjectException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.removeOneById(userId, null);
    }

    @Test
    public void testRemoveOneById() throws EmptyIdException, EmptyUserIdException, EmptyProjectException {
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        projectService.removeOneById(userId, project.getId());
    }

    @Test(expected = EmptyElementsException.class)
    public void testNegativeLoadCollectionWithoutCollection() throws EmptyElementsException {
        projectService.load((Collection<ProjectDTO>) null);
    }

    @Test
    public void testLoadCollection() throws EmptyElementsException {
        @NotNull final Collection<ProjectDTO> projects = new ArrayList<>();
        @NotNull final ProjectDTO project1 = new ProjectDTO(userId, "name1", "description");
        projects.add(project1);
        @NotNull final ProjectDTO project2 = new ProjectDTO(userId, "name2", "description");
        projects.add(project2);
        @NotNull final ProjectDTO project3 = new ProjectDTO(userId, "name3", "description");
        projects.add(project3);

        Assert.assertTrue(projectService.getList().isEmpty());
        projectService.load(projects);
        Assert.assertEquals(3, projectService.getList().size());
    }

    @Test(expected = EmptyElementsException.class)
    public void testNegativeLoadVarargWithoutVararg() throws EmptyElementsException {
        projectService.load();
    }

    @Test
    public void testLoadVararg() throws EmptyElementsException {
        @NotNull final ProjectDTO project1 = new ProjectDTO(userId, "name1", "description");
        @NotNull final ProjectDTO project2 = new ProjectDTO(userId, "name2", "description");
        @NotNull final ProjectDTO project3 = new ProjectDTO(userId, "name3", "description");

        Assert.assertTrue(projectService.getList().isEmpty());
        projectService.load(project1, project2, project3);
        Assert.assertEquals(3, projectService.getList().size());
    }

    @Test(expected = EmptyElementsException.class)
    public void testNegativeMergeOneWithoutElement() throws EmptyElementsException {
        projectService.merge((ProjectDTO) null);
    }

    @Test
    public void testMergeOne() throws EmptyElementsException {
        @NotNull final ProjectDTO project1 = new ProjectDTO(userId, "name1", "description");
        projectService.merge(project1);

        Assert.assertEquals(1, projectService.getList().size());
    }

    @Test(expected = NotExistAbstractListException.class)
    public void testNegativeMergeCollectionWithoutCollection() throws NotExistAbstractListException {
        projectService.merge((Collection<ProjectDTO>) null);
    }

    @Test
    public void testMergeCollection() throws NotExistAbstractListException {
        @NotNull final Collection<ProjectDTO> projects = new ArrayList<>();
        @NotNull final ProjectDTO project1 = new ProjectDTO(userId, "name1", "description");
        projects.add(project1);
        @NotNull final ProjectDTO project2 = new ProjectDTO(userId, "name2", "description");
        projects.add(project2);
        @NotNull final ProjectDTO project3 = new ProjectDTO(userId, "name3", "description");
        projects.add(project3);

        Assert.assertTrue(projectService.getList().isEmpty());
        projectService.merge(projects);
        Assert.assertEquals(3, projectService.getList().size());
    }

    @Test(expected = NotExistAbstractListException.class)
    public void testNegativeMergeVarargWithoutVararg() throws NotExistAbstractListException {
        projectService.merge();
    }

    @Test
    public void testMergeVararg() throws NotExistAbstractListException {
        @NotNull final ProjectDTO project1 = new ProjectDTO(userId, "name1", "description");
        @NotNull final ProjectDTO project2 = new ProjectDTO(userId, "name2", "description");
        @NotNull final ProjectDTO project3 = new ProjectDTO(userId, "name3", "description");

        Assert.assertTrue(projectService.getList().isEmpty());
        projectService.merge(project1, project2, project3);
        Assert.assertEquals(3, projectService.getList().size());
    }

    @Test
    public void testGetList() throws EmptyProjectException, EmptyUserIdException {
        @NotNull final ProjectService projectService = new ProjectService();
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        Assert.assertFalse(projectService.getList().isEmpty());
    }

    @Test
    public void testClearAll() throws EmptyProjectException, EmptyUserIdException {
        @NotNull final ProjectService projectService = new ProjectService();
        @NotNull final ProjectDTO project = new ProjectDTO(userId, "name");
        projectService.create(userId, project);
        Assert.assertFalse(projectService.getList().isEmpty());
        projectService.clearAll();
        Assert.assertTrue(projectService.getList().isEmpty());
    }

}
